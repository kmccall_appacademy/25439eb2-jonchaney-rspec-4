class Dictionary

  def initialize
    @entries = {}
  end

  def entries
    @entries
  end

  def add(entry)
    if entry.class == String
      @entries[entry] = nil
    else
      entry.each do |k,v|
        @entries[k] = v
      end
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(entry)
    @entries.include?(entry)
  end

  def find(entry)
    if @entries != {}
      @entries.select {|k,v| k.include?(entry)}
    else
      {}
    end
  end

  def printable
    printly = ""
    @entries.sort.each do |k,v|
      printly << "[#{k}] \"#{v}\""
      if k != keywords.last
        printly << "\n"
      end
    end
    printly
  end

end
