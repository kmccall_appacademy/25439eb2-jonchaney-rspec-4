class Book

  def title
    @title
  end

  def title=(title)
    @title = capitalization(title)
  end

  def capitalization(title)
    exceptions = ["a","an","and","in","of","the"]
    title.split.map.with_index do |word,idx|
      if idx == 0 || !exceptions.include?(word)
        word.capitalize
      else
        word
      end
    end.join(" ")
  end

end
