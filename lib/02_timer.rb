class Timer
  def seconds
    @seconds = 0
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def time_string()
    if @seconds < 60
      "00:00:#{"%02d" % @seconds}"
    elsif
      seconds = @seconds % 60
      minutes = @seconds / 60
      hours = 0
      if minutes > 59
        hours = minutes / 60
        minutes %= 60
      end
      "#{"%02d" % hours}:#{"%02d" % minutes}:#{"%02d" % seconds}"
    end
  end


end
