class Temperature

  def initialize(opts = {})
    @fahrenheit = opts[:f]
    @celsius = opts[:c]
  end

  # factory methods
  def self.from_celsius(celsius)
    Temperature.new(:c => celsius)
  end

  def self.from_fahrenheit(fahrenheit)
    Temperature.new(:f => fahrenheit)
  end

  # instance methods
  def in_fahrenheit
    if @fahrenheit == nil
      (@celsius * 9/5.0) + 32.0
    else
      @fahrenheit
    end
  end

  def in_celsius
    if @celsius == nil
      (@fahrenheit - 32) * (5/9.0)
    else
      @celsius
    end
  end

end

# inherited (subclass)
class Celsius < Temperature
  def initialize(celsius)
    @celsius = celsius
  end
end

class Fahrenheit < Temperature
  def initialize(fahrenheit)
    @fahrenheit = fahrenheit
  end
end


# def ftoc(fahrenheit)
#   (fahrenheit - 32) * (5/9.0)
# end
#
# def ctof(celsius)
#   (celsius * 9/5.0) + 32.0
# end
